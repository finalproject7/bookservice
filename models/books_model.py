from pymongo import MongoClient
from bson.objectid import ObjectId

class database:
    def __init__(self):
        try:
            self.nosql_db = MongoClient()
            self.db = self.nosql_db.perpustakaan
            self.mongo_col = self.db.books
            print('database connected')
        except Exception as e:
            print(e)
    
    def  showBooks(self):
        result = self.mongo_col.find()
        return [item for item in result]
    
    def showBookById(self, **params):
        result = self.mongo_col.find_one({"_id":ObjectId(params["id"])})
        return result
    
    def searchBookByName(self, **params):
        query = {"nama" : {"$regex": "{0}".format(params["nama"]), "$option" : "$i"}}
        result = self.mongo_col.find(query)
        return result
    
    def insertBook(self, document):
        self.mongo_col.insert_one(document)
    
    def search_books_id(**params):
        result = db.showBookById(**params)
        print(result)
        result["_id"] = ObjectIdToStre(result)
        return result
    
    def ubah_data(**params):
        try:
            db.updateBookById(params)
        except Exception as e:
            print(e)
        
        